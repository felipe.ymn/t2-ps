# Trabalho 2 - Projeto de Software

## Introdução

Universidade Federal do Paraná  
Disciplina: CI163 - Projeto de Software 2019/2  
Professor: Marcos Didonet  
Alunos:
- Daniel Osternack Barros Neves
- Felipe Yudi Miyoshi Nakamoto

O trabalho consiste em elaborar uma extensão da API do [sistema de ensalamento](https://gitlab.c3sl.ufpr.br/ensalamento/ensalamento-back/) desenvolvido pelo C3SL. Nessa extensão, foram realizados o projeto e implementação de um mural de informações (semelhante ao que se encontra na entrada do Dinf).

## Diagrama de Classes UML

A imagem abaixo ilustra as relações e atributos do mural de informações dentro do sistema de ensalamento:
![Diagrama de Classes UML](images/diagrama_mural.png)

## Instalação

```bash
git clone --recurse-submodules https://gitlab.com/felipe.ymn/t2-ps.git
cd t2-ps
docker-compose up
```

Para os passos seguintes, basta seguir o [README](https://gitlab.c3sl.ufpr.br/ensalamento/ensalamento-back/blob/development/README.md) do projeto original a partir da etapa "Prerequisite: install Node.js".
